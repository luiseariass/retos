function lastDayIsFriday(initialYear, endYear=0) {
    let counterFridays = 0;
    if (endYear> 0){
      for (let i = endYear ; i>initialYear - 1; i--){
        for (let j = 1; j<13 ; j++){
          if (j === 1 ||j ===  3||j === 5||j === 7||j === 8||j === 10||j === 12){
              let date = new Date (`${i}/${j}/31`)
              if (date.getDay()===5){
                counterFridays++
              }
            }
            else if( j === 4 ||j ===  6||j === 9||j === 11){ 
              let date = new Date (`${i}/${j}/30`)
              if (date.getDay()===5){
                counterFridays++
              }
            }
            else{
                if (j%4 === 0){
                    let date = new Date (`${i}/${j}/29`)
                    if (date.getDay()===5){
                    counterFridays++
                    } 
                }else{
                    let date = new Date (`${i}/${j}/28`)
                    if (date.getDay()===5){
                    counterFridays++
                    }
                }
            }
        }
    } 
                         
    }else{
        for (let j = 1; j<13 ; j++){
            if (j === 1 ||j ===  3||j === 5||j === 7||j === 8||j === 10||j === 12){
                let date = new Date (`${i}/${j}/31`)
                if (date.getDay()===5){
                  counterFridays++
                }
              }
              else if( j === 4 ||j ===  6||j === 9||j === 11){ 
                let date = new Date (`${i}/${j}/30`)
                if (date.getDay()===5){
                  counterFridays++
                }
              }
              else{
                  if (j%4 === 0){
                      let date = new Date (`${i}/${j}/29`)
                      if (date.getDay()===5){
                      counterFridays++
                      } 
                  }else{
                      let date = new Date (`${i}/${j}/28`)
                      if (date.getDay()===5){
                      counterFridays++
                      }
                  }
              }
        }   
    }
    return counterFridays
  }