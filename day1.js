function anagramCounter (wordsArray) {
    let counter = 0;
    wordsArray.forEach(function(word,indx,array) {
        let words = word.split('')
        for (let i=indx + 1;i<array.length;i++ ){
            let anagram = array[i].split('')
            let counterInterno = 0;
            if (words.length === anagram.length){
                for (let j = 0;j<words.length;j++){
                    let pos = anagram.indexOf(words[j])
                    if (pos<0){
                        break;
                    }
                    counterInterno++;
                }
            }
            if (counterInterno===words.length){
                counter++
            }
        }

    });
    return counter;
  }
console.log(anagramCounter(['dell', 'ledl', 'abc', 'cba']))  